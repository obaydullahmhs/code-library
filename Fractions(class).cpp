class Fraction {
    void simplify() {
        long long g = __gcd ( num, denom );
        num /= g;
        denom /= g;
        if ( denom < 0 ) {
            num *= -1;
            denom *= -1;
        }
    }
    long long LCM(long long a, long long b){
        return (a*b)/__gcd(a,b);
    }

public:
    long long num, denom;
    ///First deal with initiation of the class
    Fraction () {
        num = 0;
        denom = 1;
    }
    Fraction ( long long a, long long b ) {
        num = a;
        denom = b;
        simplify();
    }
    Fraction ( long long x ) {
        num = x;
        denom = 1;
    }
    Fraction ( pair<long long, long long> x ) {
        num = x.first;
        denom = x.second;
        simplify();
    }
    Fraction ( const Fraction &b ) {
        num = b.num;
        denom = b.denom;
    }
    void operator = ( long long x ) {
        num = x;
        denom = 1;
    }
    void operator = ( pair<long long, long> x ) {
        num = x.first;
        denom = x.second;
        simplify();
    }
    void operator = ( Fraction b ) {
        num = b.num;
        denom = b.denom;
    }

    ///Basic Arithmetic operations
    Fraction operator - () { ///Negation
        return Fraction( -num, denom );
    }
    Fraction operator + ( Fraction b ) { ///Addition
        Fraction res;
        res.denom = abs( LCM(denom,b.denom) );
        res.num = (res.denom/denom)*num + (res.denom/b.denom)*b.num;
        res.simplify();
        return res;
    }
    Fraction operator - ( Fraction b ) { ///Subtraction
        return (*this) + (-b);
    }
    Fraction operator * ( Fraction b ) {
        Fraction res ( b.num / __gcd(b.num,denom), b.denom / __gcd(b.denom,num) );
        res.num *= num / __gcd(num,b.denom);
        res.denom *= denom / __gcd(denom,b.num);
        res.simplify();
        return res;
    }
    Fraction operator / ( Fraction b ) { ///Division
        Fraction rev ( b.denom, b.num );
        if ( rev.denom < 0 ) {
            rev.denom *= -1;
            rev.num *= -1;
        }
        return (*this)*rev;
    }

    ///Basic Arithmetic Operations overloaded
    void operator += ( Fraction b ) {
        *this = *this + b;
    }
    void operator -= ( Fraction b ) {
        *this = *this - b;
    }
    void operator *= ( Fraction b ) {
        *this = *this * b;
    }
    void operator /= ( Fraction b ) {
        *this = *this / b;
    }

    ///Comparison
    bool operator == ( Fraction b ) {
        if ( num == b.num && denom == b.denom ) return true;
        else return false;
    }
    bool operator < ( Fraction b ) {
        if ( num * b.denom < b.num * denom ) return true;
        else return false;
    }
    bool operator > ( Fraction b ) {
        return ( b < *this );
    }
    bool operator <= ( Fraction b ) {
        if ( *this == b || *this < b ) return true;
        else return false;
    }
    bool operator >= ( Fraction b ) {
        return ( b <= *this );
    }

    ///Output
    void print() {
        printf ( "%lld/%lld %lf\n",num, denom, num/(denom-0.0) );
    }
    Fraction getAbs() {
      return Fraction(abs(num),denom);
    }
    friend ostream& operator<<(ostream& os, const Fraction& a){
        os<<a.num<<"/"<<a.denom<<"\n";
        os<<(a.num*1.0)/(1.0*a.denom)<<"\n";
        return os;
    }
};
